;;; Requires an ASDF version with package-inferred-system
(cl:unless (asdf:version-satisfies (asdf:asdf-version) "3.1.2")
  (cl:error "This library requires ASDF 3.1.2 or later."))

(asdf:defsystem #:cl-documents
  :description "Handles various text document formats"
  :version "0.0.0.0"
  :author "Michael Babich"
  :maintainer "Michael Babich"
  :license "MIT"
  :homepage "https://gitlab.com/mbabich/cl-documents"
  :bug-tracker "https://gitlab.com/mbabich/cl-documents/issues"
  :source-control (:git "https://gitlab.com/mbabich/cl-documents.git")
  :class :package-inferred-system
  :defsystem-depends-on (:asdf-package-system)
  :depends-on (:uiop
               :cl-documents/all))
