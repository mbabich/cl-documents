;;;; When complete, this file will implement the algorithm from
;;;; https://spec.commonmark.org/0.29/#appendix-a-parsing-strategy
;;;;
;;;; There are leaf blocks and container blocks. Blocks can be open or
;;;; closed.

(cl:defpackage #:cl-documents/markdown
  (:use #:cl)
  (:export #:write-markdown))

(cl:in-package #:cl-documents/markdown)

;;; The heading levels in Markdown match HTML's. They go from 1 to 6,
;;; where 1 is the highest level. This corresponds with HTML's <h1>
;;; through <h6>.
(deftype heading-level ()
  `(integer 1 6))

(defstruct markdown-block
  ;; Note: An open block can still be modified.
  (open? t :type boolean))

(defstruct (container-block (:include markdown-block))
  (children nil :type list))

(defstruct (leaf-block (:include markdown-block))
  (contents nil :type list))

(defstruct (markdown-document (:include container-block)))
(defstruct (markdown-block-quote (:include container-block)))
(defstruct (markdown-list (:include container-block)))
(defstruct (markdown-list-item (:include container-block)))

(defstruct (thematic-break (:include leaf-block)))

(defstruct (heading (:include leaf-block))
  (level nil :type heading-level))
(defstruct code-block (:include leaf-block))
(defstruct html-block (:include leaf-block))
(defstruct link-reference-definition (:include leaf-block))
(defstruct paragraph (:include leaf-block))
(defstruct blank-line (:include leaf-block))

(defun parse-line (line)
  (list line))

(defun parse-lines (stream)
  (loop :with list := (list)
        :with last := (list)
        :for line := (read-line stream nil nil)
        :for result := (parse-line line)
        :while line
        :do
           (when (listp result)
             (setf last
                   (if (endp list)
                       (setf list result)
                       (progn
                         (nconc last result)
                         result))))
        :finally (return (make-markdown-document :children list))))
