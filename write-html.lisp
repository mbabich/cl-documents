(cl:defpackage #:cl-documents/write-html
  (:nicknames #:write-html)
  (:use #:cl
        #:cl-documents/write-css)
  (:export #:custom-empty-element?
           #:write-escaped-string
           #:write-html))

(cl:in-package #:cl-documents/write-html)

(deftype html-empty-element ()
  "
Specifies the keywords that represent the tag names for all of the
empty elements.
"
  '(member :area :base :br :col :embed :hr :img :input :keygen :link :meta
    :param :source :track :wbr))

(defgeneric custom-empty-element? (tag)
  (:documentation "
A method on symbols (because keywords are just a type, not a class)
that the user can use to extend the empty element check because empty
elements must be known ahead of time.

In particular, there is a method for ((tag symbol)) that by default
always returns NIL.
"))

(defmethod custom-empty-element? ((tag symbol)) nil)

(defun write-escaped-string (string stream &optional attribute?)
  (declare (string string))
  "
This behaves like `write-string', writing string to stream, except it
uses the HTML escape sequences for <, >, and &. If attribute? is
true, then it also escapes \". The null character is replaced.
"
  (loop :for character :across string
        :do (case character
              (#\< (write-string "&lt;" stream))
              (#\> (write-string "&gt;" stream))
              (#\& (write-string "&amp;" stream))
              (#\" (if attribute?
                       (write-string "&quot;" stream)
                       (write-char character stream)))
              (#.(code-char 0)
               (write-char (code-char #xfffd) stream))
              (t (write-char character stream)))))

;;; Parses the necessary attribute information and finds where the
;;; attributes end in the s-expression body.
(defun parse-attributes (tag contents empty-element?)
  (loop :for sublist :on contents :by #'cddr
        :until (not (keywordp (car sublist)))
        ;; Attributes must be unique.
        :with seen-attributes := (make-hash-table)
        ;; If we are in a <style> tag, then the default value is
        ;; type="text/css" and we have inline CSS unless that
        ;; value is something else. Thus, if there is :type
        ;; attribute whose value is not "text/css" then this is
        ;; not inline CSS.
        :with inline-css? := (eql tag :style)
        :with comment? := (eql tag :!--)
        :unless comment?
          :do (destructuring-bind (keyword value &rest rest) sublist
                (declare (ignore rest))
                (when (gethash keyword seen-attributes)
                  (error "Attribute ~A is not unique." keyword))
                (setf (gethash keyword seen-attributes) t)
                (when (and (eql tag :style)
                           (eql keyword :type)
                           (string/= value "text/css"))
                  (setf inline-css? nil)))
        :finally (return (multiple-value-prog1 (values sublist
                                                       (cond (inline-css? :inline-css)
                                                             (comment? :comment)
                                                             (t nil)))
                           (when (and empty-element? sublist)
                             (error "Empty elements cannot have a body."))))))

(defun write-attributes (contents stream variables)
  "
There are two different obvious ways to express the attributes of
XML-like syntax in s-expression format. For example, take:

  <foo bar=\"42\">43</foo>

One way to represent this would be like this:

  (foo (:bar 42) 43)

That is, the first element is always a plist. This is easy to parse,
but it has the disadvantage of having most tags contain () or NIL as
their first element. It might be added as an optional alternative
syntax.

The other way, used here, is to have a leading plist like this:

  (foo :bar 42 43)

This is more concise, but it is not simple. A trailing plist can just
be parsed with `destructuring-bind' treating the plist as &key keyword
arguments to the lambda-list. This, however uses a leading plist.

The way this works is that it iterates through each element two at a
time, assuming that the leading plist will alternate between a keyword
and a value. The body starts at the first time that this isn't the
case.

Only the attributes are written in this function, not the body.
 "
  (loop :for sublist :on contents :by #'cddr
        :until (not (keywordp (car sublist)))
        :do (destructuring-bind (keyword value &rest rest) sublist
              (declare (ignore rest))
              (format stream " ~A=\"" (string-downcase (symbol-name keyword)))
              (let ((output (typecase value
                              (string value)
                              ((and symbol (not keyword))
                               (push value (aref variables))
                               nil)
                              (list
                               (if (eql (car value) 'function)
                                   (destructuring-bind (function function-name) value
                                     (push `(,function ,function-name) (aref variables))
                                     nil)
                                   (format nil "~A" value)))
                              (t (format nil "~A" value)))))
                (if output
                    (write-escaped-string output stream t)
                    (write-char (code-char 0) stream)))
              (write-char #\" stream))))

(defun write-html (expression &key (stream *standard-output*) (xml? nil) (variables nil) (in-paragraph? nil))
  "
Writes a list of expressions to the given stream as HTML.

If xml? is true, then it is formatted XHTML-style, e.g. <br /> instead
of <br>. If in-paragraph? is true, then it is assumed to be inside of
a <p> or similarly-behaving element for formatting purposes. If
variables is true, then it must be a 0D array containing a list of
variables. That is only for recursive calls.

This is designed to be used from the with-html macro.
"
  (declare (type (or null (simple-array t ())) variables))
  (let ((variables (if variables variables (make-array '() :initial-element (list)))))
    (flet ((write-opening-tag (tag contents xml? empty-element? comment? variables stream)
             (format stream "<~A" (string-downcase (symbol-name tag)))
             (write-attributes contents stream variables)
             (cond ((and xml? empty-element?) (write-string " />" stream))
                   ((not comment?) (write-char #\> stream))
                   (t))
             (when (member tag '(:html :head :style :body))
               (terpri stream))
             nil)
           (write-closing-tag (tag empty-element? in-paragraph? stream)
             (unless empty-element?
               (format stream "</~A>" (string-downcase (symbol-name tag))))
             (unless in-paragraph?
               (terpri stream))))
      (typecase expression
        ;; A list is either a function name or a function call or HTML.
        (list (destructuring-bind (tag &rest contents) expression
                (cond ((eql tag 'function)
                       ;; Functions are a special case of a
                       ;; variable. They are a "list" because #'foo is
                       ;; just syntactic sugar for (function foo)
                       (destructuring-bind (function-name) contents
                         (push `(function ,function-name) (aref variables))
                         (write-char (code-char 0) stream)))
                      ;; The primary HTML special case tags we have to know about are
                      ;; ones that represent an empty element.
                      ((keywordp tag)
                       (let ((empty-element? (or (typep tag 'html-empty-element)
                                                 (custom-empty-element? tag))))
                         ;; In HTML5, the DOCTYPE is only used to avoid rendering the
                         ;; document in quirks mode and provides no further information
                         ;; than this. It is required. The simplest way to handle this
                         ;; requirement is to just automatically insert it in front of
                         ;; any <html>, since there should only be one such tag.
                         (when (eql tag :html)
                           (write-line "<!DOCTYPE html>" stream))
                         ;; Find where the attributes end and the actual body begins.
                         (multiple-value-bind (body-contents special-case)
                             (parse-attributes tag contents empty-element?)
                           (write-opening-tag tag contents xml? empty-element? (eql special-case :comment) variables stream)
                           ;; Handles special cases that are determined
                           ;; from the opening tag.
                           (ecase special-case
                             (:inline-css (write-css body-contents stream #'write-escaped-string))
                             ((:comment nil)
                              (dolist (item body-contents)
                                (write-html item
                                            :stream stream
                                            :xml? xml?
                                            :variables variables
                                            :in-paragraph? (or in-paragraph?
                                                               (member tag '(:p :pre :h1 :h2 :h3 :h4 :h5 :h6)))))))
                           (if (eql special-case :comment)
                               (write-line "-->" stream)
                               (write-closing-tag tag empty-element? in-paragraph? stream)))))
                      ;; A non-keyword symbol as a "tag" is actually a
                      ;; function call.
                      ((symbolp tag)
                       (push `(,tag ,@contents) (aref variables))
                       (write-char (code-char 0) stream))
                      (t (error "An HTML list must start with a keyword or a symbol.")))))
        ;; Strings are the expected body contents.
        (string (write-escaped-string expression stream))
        ;; This is a variable, which is a special case.
        ((and symbol (not keyword))
         (push expression (aref variables))
         (write-char (code-char 0) stream))
        ;; Anything else is translated into a string and treated as a
        ;; string would be treated.
        (t (write-escaped-string (format nil "~A" expression) stream))))
    (if (aref variables) (aref variables) nil)))
