cl-documents
============

`cl-documents` is a work-in-progress name for a work-in-progress
library that handles various text document formats.

Its primary use case for now will be translating from Markdown into
HTML for static site generation. This requires generating both HTML
and CSS as well as parsing Markdown into an intermediate format, which
will be the primary initial functionality.

Do not use this library yet because the API may change in the future.
