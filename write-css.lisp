(cl:defpackage #:cl-documents/write-css
  (:nicknames #:write-css)
  (:use #:cl)
  (:export #:write-css))

(cl:in-package #:cl-documents/write-css)

;;; TODO: fancy syntax to support selector operations
;;;
;;; TODO: validate the selector string?
(defun write-selector (selector stream &optional writer)
  "
Writes a CSS selector to the given character stream with the same
writer function (or NIL) that is used in `write-css'.

The selector is either a string or a keyword.
"
  (let ((selector* (etypecase selector
                     (keyword (string-downcase (symbol-name selector)))
                     (string selector))))
    (if writer
        (funcall writer selector* stream)
        (write-string selector* stream))))

;;; TODO: support variables and functions
(defun write-css (css-rules stream &optional writer)
  "
Writes a list of CSS rules to the given character stream. Each CSS
rule is itself a list. The first item of a css-rule is either a
selector list or a selector. A selector list is a list that begins
with :sl. Everything else is a selector. The remaining items in a
css-rule are the body of the CSS definition.

The variable writer is either NIL or a function. If writer is NIL,
then `write-string' is used for the selector and body. If writer is
not NIL, then it is called with the item to write as the first
argument and the stream to write to as the second. This is so
character escape rules can be applied while writing, such as when CSS
is embedded within an HTML argument. This will not escape the newline
or the characters '{', '}', ';', and ',' because these are not part of
the CSS rule selector or rule body.
"
  (let ((writer (if writer writer #'write-string)))
    (dolist (css-rule css-rules)
      (destructuring-bind (selector &rest properties) css-rule
        ;; Write selector or selector list.
        (if (and (listp selector) (eql (car selector) :sl))
            (loop :for selectors :on (cdr selector)
                  :do (write-selector (car selectors) stream writer)
                  :unless (endp (cdr selectors))
                    :do (write-string ", " stream))
            (write-selector selector stream writer))
        ;; Write rule body
        (write-line " {" stream)
        (loop :for sublist :on properties :by #'cddr
              :do (destructuring-bind (key value &rest rest) sublist
                    (declare (ignore rest))
                    (write-string "  " stream)
                    (funcall writer
                             (etypecase key
                               (keyword (string-downcase (symbol-name key)))
                               (string key))
                             stream)
                    (write-string ": " stream)
                    (funcall writer value stream)
                    (write-line ";" stream)))
        (write-line "}" stream)
        (terpri stream)))))
