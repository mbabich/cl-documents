(cl:defpackage #:cl-documents/example
  (:use #:cl
        #:cl-documents/generate-html
        #:cl-documents/write-html)
  (:export #:example))

(cl:in-package #:cl-documents/example)

(defun write-head (title &key stream)
  (with-html (stream)
    (:head (:title title)
           ;; A blank favicon instead of a missing one.
           (:link :href "data:," :rel "icon")
           (:style :type "text/css"
                   (:body :font-family "sans-serif" :font-size "10pt"
                          :max-width "650px" :margin "40px auto" :color "#000")
                   (:pre :font-family "monospace" :font-size "9pt"
                         :color "#fff" :background-color "#000"
                         :max-height "600px"
                         :overflow-x "scroll" :overflow-y "scroll")
                   (:code :font-family "monospace")))))

;;; A simple example of the kind of HTML templates that can be made
;;; using macros.
(defmacro with-simple-html-template ((stream &key title) &body body)
  `(with-html (,stream :top-level? t)
     (write-head ,title :stream ,stream)
     (:body ,@body)))

(defun document (package &key stream)
  (flet ((name (symbol &key stream)
           (write-escaped-string (symbol-name symbol) stream))
         ;; A symbol that is fboundp can be a macro, a special
         ;; operator, or a function. This writes which one.
         (function-or-macro (symbol &key stream)
           (write-escaped-string (cond ((macro-function symbol) "macro")
                                       ((special-operator-p symbol) "special operator")
                                       (t "function"))
                                 stream))
         ;; Naively parse docstrings
         (docs (symbol &key stream)
           (let ((documentation (documentation symbol 'function)))
             (if documentation
                 (with-input-from-string (symbol documentation)
                   (write-line "<p>" stream)
                   (loop :for line := (read-line symbol nil)
                         :with written-line? := nil
                         :while line
                         :if (string= line "")
                           :do
                              (when written-line?
                                (write-line "</p>" stream)
                                (write-string "<p>" stream))
                         :else
                           :do
                              (write-char #\Space stream)
                              (write-escaped-string line stream)
                              (setf written-line? t)
                         :finally (write-line "</p>" stream)))
                 (with-html (stream)
                   (:em "No docstring."))))))
    (do-external-symbols (s package)
      ;; If it's in the function namespace, then it is either a
      ;; function or a macro.
      ;;
      ;; TODO: handle variables, constants, types, classes, etc.
      (when (fboundp s)
        ;; TODO: retrieve the lambda list
        (with-html (stream)
          (:h3 (:code (name s)) " (" (:em (function-or-macro s)) ")"))
        (with-html (stream)
          (docs s))))))

(defun random-number (&key stream)
  (format stream "~A" (random 42)))

(defun example-html (stream)
  (let ((example "https://example.com/")
        (title "Sample document"))
    (with-simple-html-template (stream :title title)
      (:!-- "This is a comment! " (:p "Even HTML can be commented."))
      (:h1 title)
      (:h2 "Sample section")
      (:p "This is " (:em "the") " " (:strong "message") " body.")
      (:p "This is a link to " (:a :href example "Example.com"))
      (:p "A paragraph uses a " (:code "<p>") " tag" (:br) "& everything goes inside of the "
          (:code "<html>") " tag.")
      (:h3 "A subsection")
      (:p "Your random number for today is " (:strong #'random-number) ".")
      (:p "This is a paragraph that is very long. "
          "It is long enough that it wraps all the way onto the next line! "
          "Websites that don't wrap like this are unusable on widescreens! "
          "In fact, let's make it wrap one more time! (But only with some fonts.)")
      (:h2 "Code")
      (:h3 (:code "example.lisp"))
      (:pre (:code "(defun foo ()
  (format t \"Hello, world!\")"))
      (:p "The above is a test of displaying code in the browser!")
      (:h2 "Documentation")
      (document :cl-documents))))

;;; TODO: Add an alternate syntax mode that always has an attribute
;;; list (usually empty) instead of determining what the attributes
;;; are?
;;;
;;; TODO: Support variables in CSS
;;;
;;; TODO: Markdown->intermediate-format->HTML
(defun example (&optional stream)
  (if stream
      (example-html stream)
      (with-output-to-string (output) (example-html output))))
