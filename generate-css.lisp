(cl:defpackage #:cl-documents/generate-css
  (:nicknames #:generate-css)
  (:use #:cl
        #:cl-documents/write-css)
  (:export #:with-css))

(cl:in-package #:cl-documents/generate-css)

(defmacro with-css ((stream) &body body)
  `(progn
     (write-string ,(with-output-to-string (s) (write-css body s nil)) ,stream)
     nil))
