(cl:defpackage #:cl-documents/generate-html
  (:nicknames #:generate-html)
  (:use #:cl
        #:cl-documents/write-html)
  (:export #:with-html))

(cl:in-package #:cl-documents/generate-html)

(defmacro with-html ((stream &key (top-level? nil)) &body body)
  "
Translates an s-expression representation of HTML into a series of
write/print/format/etc. calls on the given character stream. If
top-level? then it implicitly inserts <html>...</html> around the
body.

If a valid CSS <style> element is encountered, then it switches to the
CSS generation mode instead, as if called with with-css, except with
the necessary HTML characters escaped. If this is undesirable, then
use a function (see below) to generate it manually.

If it encounters a variable (a non-keyword symbol not at the start of
a list) or a function name (with the #'foo or (function foo) syntax),
then it handles those as special cases. For variables, `write-html' is
called at runtime on those variables, using the same stream as the
macro. This could be expensive.

For functions, the functions are called with keyword arguments,
including at least :stream. There is also an unknown number of
additional keyword arguments, with :allow-other-keys t, making
additions to the keyword arguments not cause breaking changes in the
user code. The functions cannot take any context-dependent arguments
because only the name is provided, so it's recommended that closures
are created with `flet' before use.
"
  (let* ((body* (if top-level?
                    `(:html ,@body)
                    (if (cdr body)
                        (error "Too many elements for non-top-level html")
                        (car body))))
         ;; Write the HTML to a string and save the variables as a
         ;; list using SETF.
         (variables nil)
         (output (with-output-to-string (s)
                   (setf variables (reverse (write-html body* :stream s)))))
         ;; Find the position of all of the characters that mean that
         ;; there is a variable in this location. (code-char 0) is
         ;; used for that.
         (positions (loop :for start := 0 :then (1+ position)
                          :for position := (position (code-char 0)
                                                     output
                                                     :start start)
                          :while position
                          :collect position)))
    `(progn
       ;; Go over every position and variable. At the position, write
       ;; the variable or call the function instead of writing the
       ;; string. Otherwise, write the string.
       ,@(loop :for sublist := positions :then (cdr sublist)
               :for var-sublist := variables :then (cdr var-sublist)
               :for position := (car sublist)
               :for variable := (car var-sublist)
               :for start := 0 :then (1+ end)
               :for end := position
               :when (numberp end)
                 :when (not (= start end))
                   :collect `(write-string ,(subseq output start end) ,stream)
               :end
               ;; Insert the variables or function calls between
               ;; substrings.
               :and :collect (etypecase variable
                               (symbol `(write-html ,variable :stream ,stream))
                               (list (if (eql (car variable) 'function)
                                         (destructuring-bind (function function-name) variable
                                           (declare (ignore function))
                                           ;; Note: Other keys can be added here.
                                           `(,function-name :stream ,stream :allow-other-keys t))
                                         (destructuring-bind (function-name &rest args) variable
                                           `(,function-name ,@args :stream ,stream :allow-other-keys t)))))
               :when (null end)
                 :collect `(write-string ,(if (zerop start)
                                              output
                                              (subseq output start))
                                         ,stream)
               :while (numberp end))
       nil)))
