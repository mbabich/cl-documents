(uiop:define-package #:cl-documents/all
  (:nicknames :cl-documents)
  (:use #:cl)
  (:use-reexport #:cl-documents/generate-css
                 #:cl-documents/generate-html
                 #:cl-documents/write-css
                 #:cl-documents/write-html
                 #:cl-documents/markdown
                 #:cl-documents/example))
